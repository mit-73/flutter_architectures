import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:redux/redux.dart';
import 'package:redux_sample/app.dart';
import 'package:redux_sample/reducers/app_state_reducer.dart';
import 'package:todos_repository_simple/todos_repository_simple.dart';

import 'middleware/store_todos_middleware.dart';
import 'models/app_state.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(ReduxApp(
    store: Store<AppState>(
      appReducer,
      initialState: AppState.loading(),
      middleware: createStoreTodosMiddleware(
        TodosRepositoryFlutter(
          fileStorage: FileStorage(
            'redux_local_storage',
            getApplicationDocumentsDirectory,
          ),
          webClient: WebClient(),
        ),
      ),
    ),
  ));
}
