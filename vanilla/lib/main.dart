import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:todos_repository_simple/todos_repository_simple.dart';
import 'package:vanilla/app.dart';

void main() {
  runApp(
    VanillaApp(
      repository: TodosRepositoryFlutter(
        fileStorage: FileStorage(
          "vanilla_local_storage",
          getApplicationDocumentsDirectory,
        ),
        webClient: WebClient(),
      ),
    ),
  );
}
