import 'dart:async';

import 'models/models.dart';
import 'todos_interactor.dart';

class TodoBloc {
  final TodosInteractor _interactor;

  TodoBloc(TodosInteractor interactor) : _interactor = interactor;

  // Inputs
  void deleteTodo(String id) => _interactor.deleteTodo(id);

  void updateTodo(Todo todo) => _interactor.updateTodo(todo);

  // Outputs
  Stream<Todo> todo(String id) => _interactor.todo(id);
}
