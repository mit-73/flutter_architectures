import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:todos_app_core/todos_app_core.dart';
import 'package:todos_repository_core/todos_repository_core.dart';

import 'blocs/todos_interactor.dart';
import 'blocs/todos_list_bloc.dart';
import 'dependency_injection.dart';
import 'localization.dart';
import 'screens/add_edit_screen.dart';
import 'screens/home_screen.dart';
import 'widgets/todos_bloc_provider.dart';

void runSimpleBlocApp({
  @required TodosInteractor todosInteractor,
  @required UserRepository userRepository,
}) {
  runApp(Injector(
    todosInteractor: todosInteractor,
    userRepository: userRepository,
    child: TodosBlocProvider(
      bloc: TodosListBloc(todosInteractor),
      child: MaterialApp(
        title: BlocLocalizations().appTitle,
        theme: ArchSampleTheme.theme,
        localizationsDelegates: [
          ArchSampleLocalizationsDelegate(),
          InheritedWidgetLocalizationsDelegate(),
        ],
        routes: {
          ArchSampleRoutes.home: (context) {
            return HomeScreen(
              repository: Injector.of(context).userRepository,
            );
          },
          ArchSampleRoutes.addTodo: (context) {
            return AddEditScreen(
              addTodo: TodosBlocProvider.of(context).addTodo,
            );
          },
        },
      ),
    ),
  ));
}
