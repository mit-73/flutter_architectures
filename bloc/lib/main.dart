import 'dart:async';

import 'package:path_provider/path_provider.dart';
import 'package:todos_repository_core/todos_repository_core.dart';
import 'package:todos_repository_simple/todos_repository_simple.dart';
import 'blocs/todos_interactor.dart';
import 'run_simple_bloc_app.dart';

void main() {
  runSimpleBlocApp(
    todosInteractor: TodosInteractor(
      ReactiveTodosRepositoryFlutter(
        repository: TodosRepositoryFlutter(
          fileStorage: FileStorage(
            'bloc_local_storage',
            getApplicationDocumentsDirectory,
          ),
          webClient: WebClient(),
        ),
      ),
    ),
    userRepository: AnonymousUserRepository(),
  );
}

class AnonymousUserRepository implements UserRepository {
  @override
  Future<UserEntity> login() {
    return Future.value(UserEntity(id: 'anonymous'));
  }
}
