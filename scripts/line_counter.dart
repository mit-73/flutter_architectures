import 'dart:io';

class Sample {
  final String name;
  final List<Directory> directories;

  Sample(this.name, List<String> paths)
      : directories = paths.map((path) => Directory('$path/lib')).toList();
}

class Output {
  final String name;
  final int lineCount;

  Output(this.name, this.lineCount);

  @override
  String toString() {
    return 'Output{name: $name, lineCount: $lineCount}';
  }
}

void main() {
  final samples = [
    Sample('  core', [
      'todos_app_core',
      'todos_repository_core',
      'todos_repository_simple',
      'varp_package',
    ]),
    Sample(' tests', ['todo_integration_tests']),
    Sample('  bloc', ['bloc']),
    Sample('vanilla', ['vanilla']),
    Sample(' redux', ['redux']),
    Sample(' varp  ', ['varp']),
  ];
  final outputs = samples.map<Output>((sample) {
    return Output(
      sample.name,
      _countLines(sample.directories),
    );
  }).toList(growable: false)
    ..sort((a, b) => a.lineCount - b.lineCount);

  final strings = outputs
      .map<String>(
          (output) => '| ${output.name}\t | ${output.lineCount}\t\t\t |')
      .join('\n');

  print('''
# Line Counts (${DateTime.now().toUtc()})

| *Sample*\t | *LOC (no comments)*\t |
------------------------------------------
$strings 
''');
}

int _countLines(List<Directory> directories) {
  final List<File> dartFiles = _findDartFiles(directories);

  return dartFiles.fold(0, (count, file) {
    final nonCommentsLineCount = file
        .readAsLinesSync()
        .where((line) => !line.startsWith('//') && line.trim().isNotEmpty)
        .length;

    return count + nonCommentsLineCount;
  });
}

List<File> _findDartFiles(List<Directory> directories) {
  final paths = directories.fold(<String>{}, (files, directory) {
    final currentDirectoryDartFiles = directory
        .listSync(recursive: true)
        .whereType<File>()
        .map((file) => file.path)
        .where((path) => path.endsWith('.dart') && !path.endsWith('.g.dart'))
        .toSet();

    return {...files, ...currentDirectoryDartFiles};
  });

  return List.unmodifiable(paths.map<File>((path) => File(path)));
}
