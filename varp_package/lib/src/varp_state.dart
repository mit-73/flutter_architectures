import 'dart:async';

import 'varp_core.dart';

class VarpState {
  final StreamController<VarpCore> _state = StreamController<VarpCore>();
  Sink<VarpCore> get _inState => _state.sink;
  Stream<VarpCore> get outState => _state.stream;
  VarpCore _currentState;

  VarpState() {
    _addCurrentStateToStream();
  }

  void setState(VarpCore state) {
    assert(state != null);
    _currentState = state;
    _addCurrentStateToStream();
  }

  Future<void> nextState() async {
    await _currentState.nextState(this);
  }

  void dispose() {
    _state.close();
  }

  void _addCurrentStateToStream() {
    _inState.add(_currentState);
  }
}
