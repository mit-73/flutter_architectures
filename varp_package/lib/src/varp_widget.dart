import 'package:flutter/widgets.dart';

import 'varp_core.dart';
import 'varp_property.dart';
import 'varp_state.dart';

abstract class VarpWidget extends Widget implements VarpCore {
  @override
  void initState() {}

  @override
  void dispose() {}

  @override
  Future nextState(VarpState state) async {
    state.setState(this);
  }

  @override
  VarpState state = VarpState();

  @override
  VarpElement createElement() => VarpElement(this);
}

class VarpElement extends ComponentElement {
  VarpElement(VarpWidget widget) : super(widget);

  final VarpProperties _properties = VarpProperties();

  @override
  VarpWidget get widget => super.widget;

  @override
  Widget build() {
    return StreamBuilder(
      stream: widget.state.outState,
      builder: (BuildContext context, AsyncSnapshot<VarpCore> snapshot) {
        if (snapshot.hasData && !snapshot.hasError)
          return snapshot.data.build(context);
        else
          return widget.build(context);
      },
    );
  }

  @override
  void mount(Element parent, newSlot) {
    _properties.add(widget);
    widget.initState();
    super.mount(parent, newSlot);
  }

  @override
  void unmount() {
    widget.dispose();
    widget.state.dispose();
    _properties.remove(widget);
    super.unmount();
  }

  @override
  void update(VarpWidget newWidget) {
    newWidget.state = widget.state;
    super.update(newWidget);
  }
}
