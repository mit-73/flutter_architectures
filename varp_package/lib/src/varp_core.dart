import 'package:flutter/widgets.dart' show Widget, BuildContext;

import 'varp_property.dart';
import 'varp_state.dart';

abstract class VarpCore<T extends VarpProperty, E extends VarpState> {
  T property;
  E state;

  void initState();

  Widget build(BuildContext context);

  Future nextState(E state);

  void dispose();
}
