import 'varp_core.dart';

class VarpProperty {
  void Function(int hashCode) _callback;

  void varpPropertyChanged(int hashCode) {
    if (_callback != null) {
      _callback(hashCode);
    }
  }
}

class VarpProperties {
  static final VarpProperties _singleton = VarpProperties._internal();
  factory VarpProperties() => _singleton;
  VarpProperties._internal();

  List<VarpCore> _varpProperties = List<VarpCore>();

  void add(VarpCore varpCore) {
    if (!_varpProperties.contains(varpCore)) {
      varpCore.property._callback = propertyChanged;
      _varpProperties.add(varpCore);
    }
  }

  void remove(VarpCore varpCore) {
    if (_varpProperties.contains(varpCore)) {
      _varpProperties.remove(varpCore);
    }
  }

  void propertyChanged(int hashCode) {
    for (int i = 0; i < _varpProperties.length; i++) {
      if (_varpProperties[i].property.hashCode == hashCode)
        _varpProperties[i].nextState(_varpProperties[i].state);
    }
  }
}
