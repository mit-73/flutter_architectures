import 'dart:async';

import 'package:todos_repository_core/todos_repository_core.dart';

/// A class that is meant to represent a Client that would be used to call a Web
/// Service. It is responsible for fetching and persisting Todos to and from the
/// cloud.
class WebClient {
  final Duration delay;

  const WebClient([this.delay = const Duration(milliseconds: 3000)]);

  Future<List<TodoEntity>> fetchTodos() async {
    return Future.delayed(
        delay,
        () => [
              TodoEntity(
                'Test 1',
                '1',
                'Desc 1',
                false,
              ),
              TodoEntity(
                'Test',
                '2',
                'Text',
                false,
              ),
              TodoEntity(
                'ABC',
                '3',
                '',
                true,
              ),
              TodoEntity(
                '',
                '4',
                '',
                false,
              ),
              TodoEntity(
                'null',
                '5',
                'null',
                true,
              ),
            ]);
  }

  Future<bool> postTodos(List<TodoEntity> todos) async {
    return Future.value(true);
  }
}
