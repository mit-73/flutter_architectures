import 'package:flutter/material.dart';

class ArchSampleTheme {
  static ThemeData get theme {
    return ThemeData.light();
  }
}
