import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = MessageLookup();

// ignore: unused_element
final _keepAnalysisHappy = Intl.defaultLocale;

// ignore: non_constant_identifier_names
typedef MessageIfAbsent(String message_str, List args);

class MessageLookup extends MessageLookupByLibrary {
  get localeName => 'ru';

  static m0(task) => "Удалить \"${task}\"";

  final messages = _notInlinedMessages(_notInlinedMessages);

  static _notInlinedMessages(_) => {
        "activeTodos": MessageLookupByLibrary.simpleMessage("Активные задачи"),
        "addTodo": MessageLookupByLibrary.simpleMessage("Добавить все"),
        "cancel": MessageLookupByLibrary.simpleMessage("Отмена"),
        "clearCompleted":
            MessageLookupByLibrary.simpleMessage("Очистить завершеные"),
        "completedTodos":
            MessageLookupByLibrary.simpleMessage("Завершеные задачи"),
        "delete": MessageLookupByLibrary.simpleMessage("Удалить"),
        "deleteTodo": MessageLookupByLibrary.simpleMessage("Удалить все"),
        "deleteTodoConfirmation":
            MessageLookupByLibrary.simpleMessage("Удалить это задачу?"),
        "editTodo": MessageLookupByLibrary.simpleMessage("Изменить все"),
        "emptyTodoError":
            MessageLookupByLibrary.simpleMessage("Пожалуйста, введите текст"),
        "filterTodos": MessageLookupByLibrary.simpleMessage("Фильтровать"),
        "markAllComplete":
            MessageLookupByLibrary.simpleMessage("Отметить все выполнено"),
        "markAllIncomplete": MessageLookupByLibrary.simpleMessage(
            "Отметить все как невыполненые"),
        "newTodoHint":
            MessageLookupByLibrary.simpleMessage("Что должно быть сделано?"),
        "notesHint": MessageLookupByLibrary.simpleMessage("Дополнительно"),
        "saveChanges":
            MessageLookupByLibrary.simpleMessage("Сохранить изменения"),
        "showActive": MessageLookupByLibrary.simpleMessage("Показать активные"),
        "showAll": MessageLookupByLibrary.simpleMessage("Показать все"),
        "showCompleted":
            MessageLookupByLibrary.simpleMessage("Показать выполненые"),
        "stats": MessageLookupByLibrary.simpleMessage("Статистика"),
        "todoDeleted": m0,
        "todoDetails": MessageLookupByLibrary.simpleMessage("Детали задачи"),
        "todos": MessageLookupByLibrary.simpleMessage("Задачи"),
        "undo": MessageLookupByLibrary.simpleMessage("Отменить")
      };
}
