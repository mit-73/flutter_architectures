import 'dart:async';

import 'package:flutter/material.dart';

class VarpLocalizations {
  static VarpLocalizations of(BuildContext context) {
    return Localizations.of<VarpLocalizations>(context, VarpLocalizations);
  }

  String get appTitle => "Varp Example";
}

class VarpLocalizationsDelegate
    extends LocalizationsDelegate<VarpLocalizations> {
  @override
  Future<VarpLocalizations> load(Locale locale) =>
      Future(() => VarpLocalizations());

  @override
  bool shouldReload(VarpLocalizationsDelegate old) => false;

  @override
  bool isSupported(Locale locale) =>
      locale.languageCode.toLowerCase().contains("en");
}
