import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:todos_repository_simple/todos_repository_simple.dart';
import 'package:varp/app.dart';

void main() {
  runApp(
    VarpApp(
      TodosRepositoryFlutter(
        fileStorage: FileStorage(
          "varp_local_storage",
          getApplicationDocumentsDirectory,
        ),
        webClient: WebClient(),
      ),
    ),
  );
}
