import 'package:flutter/material.dart';
import 'package:varp_package/varp.dart';
import 'package:todos_app_core/todos_app_core.dart';
import 'package:todos_repository_core/todos_repository_core.dart';
import 'package:varp/localization.dart';
import 'package:varp/models.dart';
import 'package:varp/screens/add_edit_screen.dart';
import 'package:varp/screens/home_screen.dart';

class VarpApp extends VarpWidget {
  final TodosRepository repository;

  VarpApp(this.repository) {
    property = _ViewModel(repository: this.repository);
  }

  @override
  VarpProperty property = _ViewModel();
  _ViewModel get vm => property;

  @override
  void initState() {
    vm.load();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: VarpLocalizations().appTitle,
      theme: ArchSampleTheme.theme,
      localizationsDelegates: [
        ArchSampleLocalizationsDelegate(),
        VarpLocalizationsDelegate(),
      ],
      routes: {
        ArchSampleRoutes.home: (context) {
          return HomeScreen(
            appState: vm.appState,
            updateTodo: vm.updateTodo,
            addTodo: vm.addTodo,
            removeTodo: vm.removeTodo,
            toggleAll: vm.toggleAll,
            clearCompleted: vm.clearCompleted,
            updateTab: vm.updateTab,
            updateVisibility: vm.updateVisibility,
            activeFilter: vm.activeFilter,
            activeTab: vm.activeTab,
          );
        },
        ArchSampleRoutes.addTodo: (context) {
          return AddEditScreen(
            key: ArchSampleKeys.addTodoScreen,
            addTodo: vm.addTodo,
            updateTodo: vm.updateTodo,
          );
        },
      },
    );
  }
}

class _ViewModel extends VarpProperty {
  final TodosRepository repository;
  _ViewModel({this.repository});

  AppState _appState = AppState.loading();

  AppState get appState => _appState;

  set appState(AppState appState) {
    _appState = appState;
  }

  void _save() {
    repository
        .saveTodos(appState.todos.map((todo) => todo.toEntity()).toList());

    varpPropertyChanged(this.hashCode);
  }

  void load() {
    repository.loadTodos().then((loadedTodos) {
      appState = AppState(todos: loadedTodos.map(Todo.fromEntity).toList());
      _save();
    }).catchError((err) {
      appState.isLoading = false;
      _save();
    });
  }

  void toggleAll() {
    appState.toggleAll();

    _save();
  }

  void clearCompleted() {
    appState.clearCompleted();

    _save();
  }

  void addTodo(Todo todo) {
    appState.todos.add(todo);

    _save();
  }

  void removeTodo(Todo todo) {
    appState.todos.remove(todo);

    _save();
  }

  void updateTodo(
    Todo todo, {
    bool complete,
    String id,
    String note,
    String task,
  }) {
    todo.complete = complete ?? todo.complete;
    todo.id = id ?? todo.id;
    todo.note = note ?? todo.note;
    todo.task = task ?? todo.task;

    _save();
  }

  VisibilityFilter _activeFilter = VisibilityFilter.all;

  VisibilityFilter get activeFilter => _activeFilter;

  set activeFilter(VisibilityFilter activeFilter) {
    _activeFilter = activeFilter;
  }

  AppTab _activeTab = AppTab.todos;

  AppTab get activeTab => _activeTab;

  set activeTab(AppTab activeTab) {
    _activeTab = activeTab;
  }

  updateVisibility(VisibilityFilter filter) {
    activeFilter = filter;

    varpPropertyChanged(this.hashCode);
  }

  updateTab(AppTab tab) {
    activeTab = tab;

    varpPropertyChanged(this.hashCode);
  }
}
