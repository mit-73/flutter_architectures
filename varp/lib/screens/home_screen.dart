import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:todos_app_core/todos_app_core.dart';
import 'package:varp/localization.dart';
import 'package:varp/models.dart';
import 'package:varp/widgets/extra_actions_button.dart';
import 'package:varp/widgets/filter_button.dart';
import 'package:varp/widgets/stats_counter.dart';
import 'package:varp/widgets/todo_list.dart';
import 'package:varp/widgets/typedefs.dart';

class HomeScreen extends StatelessWidget {
  final AppState appState;
  final TodoAdder addTodo;
  final TodoRemover removeTodo;
  final TodoUpdater updateTodo;
  final Function toggleAll;
  final Function clearCompleted;
  final VisibilityFilter activeFilter;
  final AppTab activeTab;
  final Function updateVisibility;
  final Function updateTab;

  HomeScreen({
    @required this.appState,
    @required this.addTodo,
    @required this.removeTodo,
    @required this.updateTodo,
    @required this.toggleAll,
    @required this.clearCompleted,
    @required this.activeFilter,
    @required this.activeTab,
    @required this.updateVisibility,
    @required this.updateTab,
    Key key,
  }) : super(key: ArchSampleKeys.homeScreen);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(VarpLocalizations.of(context).appTitle),
        actions: [
          FilterButton(
            isActive: activeTab == AppTab.todos,
            activeFilter: activeFilter,
            onSelected: updateVisibility,
          ),
          ExtraActionsButton(
            allComplete: appState.allComplete,
            hasCompletedTodos: appState.hasCompletedTodos,
            onSelected: (action) {
              if (action == ExtraAction.toggleAllComplete) {
                toggleAll();
              } else if (action == ExtraAction.clearCompleted) {
                clearCompleted();
              }
            },
          )
        ],
      ),
      body: activeTab == AppTab.todos
          ? TodoList(
              filteredTodos: appState.filteredTodos(activeFilter),
              loading: appState.isLoading,
              removeTodo: removeTodo,
              addTodo: addTodo,
              updateTodo: updateTodo,
            )
          : StatsCounter(
              numActive: appState.numActive,
              numCompleted: appState.numCompleted,
            ),
      floatingActionButton: FloatingActionButton(
        key: ArchSampleKeys.addTodoFab,
        onPressed: () {
          Navigator.pushNamed(context, ArchSampleRoutes.addTodo);
        },
        child: Icon(Icons.add),
        tooltip: ArchSampleLocalizations.of(context).addTodo,
      ),
      bottomNavigationBar: BottomNavigationBar(
        key: ArchSampleKeys.tabs,
        currentIndex: AppTab.values.indexOf(activeTab),
        onTap: (index) {
          updateTab(AppTab.values[index]);
        },
        items: AppTab.values.map((tab) {
          return BottomNavigationBarItem(
            icon: Icon(
              tab == AppTab.todos ? Icons.list : Icons.show_chart,
              key: tab == AppTab.stats
                  ? ArchSampleKeys.statsTab
                  : ArchSampleKeys.todoTab,
            ),
            title: Text(
              tab == AppTab.stats
                  ? ArchSampleLocalizations.of(context).stats
                  : ArchSampleLocalizations.of(context).todos,
            ),
          );
        }).toList(),
      ),
    );
  }
}
